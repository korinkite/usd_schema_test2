## How To Build

```bash
export USD_ROOT_PATH=/usr/local/USD
export LIBRARY_PATH=$USD_ROOT_PATH/lib
cd build
PATH=/folder/to/usdGenSchema cmake ..
make
```

Note: Update the `export` commands to point to the correct locations


## TODO
- Find a way to export the build path so that other CMake projects can
- use it change the build directory for the .so. It should be separate
  from the source files.
- Check if I need to export plugInfo.json / generatedSchema.json
